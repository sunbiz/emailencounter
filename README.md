emailencounter
===================
An OpenMRS module to create encounters from information in emails. 
The primary objective is to distribute forms over email as attachments or embedded in email and sent to an email address. 
The module downloads the email and creates an encounter from the email