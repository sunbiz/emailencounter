package org.openmrs.module.emailencounter.web.extension;

import java.util.LinkedHashMap;
import java.util.Map;

import org.openmrs.module.web.extension.AdministrationSectionExt;

/**
 * Displays the admin section on the admin index page
 */
public class AdminList extends AdministrationSectionExt {

    @Override
    public Map<String, String> getLinks() {

        Map<String, String> links = new LinkedHashMap<String, String>();
        return links;
    }

    @Override
    public String getTitle() {
        return "Email Encounter Module";
    }

}
